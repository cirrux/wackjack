from random import SystemRandom

cardNames = {2:'2',3:'3',4:'4',5:'5',6:'6',7:'7',8:'8',9:'9',10:'10',11:'Jack',12:'Queen',13:'King',14:'Ace'}
cardValues = {2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,10:10,11:10,12:10,13:10,14:11}
suitNames = {'H':'Hearts','D':'Diamonds','C':'Clubs','S':'Spades'}
cardPoints = {}

decks = 1

def hand_win_bust(hv):
        hand_val = hv[0]
        aces = hv[1]
        # 1 - win, 2 - bust
        if hand_val > 21:
                return 2
        if hand_val == 21:
                return 1 if aces == 0 else 2
        if hand_val == 10 and aces == 1:
                return 1
        if hand_val > 10:
                if aces == (21 - hand_val):
                        return 1
                elif aces > (21 - hand_val):
                        return 2
        return 0

def end_hand_val(hand_val, aces):
        if hand_val == 21 and aces == 0:
                return 21
        if hand_val == 10:
                if aces == 1:
                        return 21
                else:
                        return hand_val + aces
        if hand_val > 10:
                return hand_val + aces
        if hand_val < 10:
                total = hand_val
                for ace in range(0, aces):
                        if (total + 11) > 21:
                                total += 1
                        else:
                                total += 11
                return total
        return hand_val
        

class Game():
        def __init__(self):
                self.rand = SystemRandom()
                print("Welcome to Blackjack")
                self.player = User()
                self.ai = AI()
                self.new_game()

        def new_game(self):
                self.deck = Deck()
                self.new_round()

        def new_round(self):
                self.player.reset()
                self.ai.reset()
                bet = input("Your balance is $%s, enter a bet to begin: " % self.player.wallet)
                while not bet.isdigit() or int(bet) > self.player.wallet:
                        bet = input("You must enter a round number smaller than your balance: ")
                print("----------------------------------------------------------")
                self.player.bet = int(bet)
                print("[Drawing cards...]")
                self.player.add_cards(self.get_card(2))
                if hand_win_bust(self.player.hand_value()) > 0:
                        self.end_round(True, False, True) if hand_win_bust(self.player.hand_value()) == 1 else self.end_round(False)
                        return
                self.ai.add_cards(self.get_card(2))
                self.ai.do_show(1)
                print("----------------------------------------------------------")
                print("[Your turn to play...]")
                self.get_player_move()

        def get_player_move(self):
                hand_val, aces = self.player.hand_value()
                d_hand_val, d_aces = self.ai.hand_value()
                print("Your hand value is %s, plus %s ace" % (hand_val, aces) if aces > 0 else "Your hand value is %s" % hand_val)
                print("The dealers shown hand value is %s, plus %s ace" % (d_hand_val, d_aces) if d_aces > 0 else "The dealers hand value is %s" % d_hand_val)
                the_move = input("What do you want to do (hit, stand, double, split)? ")
                while the_move not in ['hit', 'stand', 'double', 'split']:
                        the_move = input("You must choose hit, stand, double or split: ")
                print("----------------------------------------------------------")
                self.process_move(self.player, the_move)

        def process_move(self, mover, move):
                print("[%s decided to %s]" % ("You" if mover == self.player else "The dealer", move))
                if move == 'hit':
                        self.player.add_cards(self.get_card(1))
                        if hand_win_bust(self.player.hand_value()) > 0:
                                self.end_round(True) if hand_win_bust(self.player.hand_value()) == 1 else self.end_round(False)
                                return
                        self.get_player_move()
                elif move == 'stand':
                        hand_val, aces = self.player.hand_value()
                        print(("Your hand value is %s, plus %s ace" % (hand_val, aces) if aces > 0 else "Your hand value is %s" % hand_val))
                        print("----------------------------------------------------------")
                        self.start_dealer_play()
                elif move == 'double':
                        self.player.bet *= 2
                        print("Your bet is now doubled to $%s" % self.player.bet)
                        self.player.add_cards(self.get_card(1))
                        if hand_win_bust(self.player.hand_value()) > 0:
                                self.end_round(True) if hand_win_bust(self.player.hand_value()) == 1 else self.end_round(False)
                                return
                        hand_val, aces = self.player.hand_value()
                        print(("Your hand value is %s, plus %s ace" % (hand_val, aces) if aces > 0 else "Your hand value is %s" % hand_val))
                        print("----------------------------------------------------------")
                        self.start_dealer_play()
                elif move == 'split':
                        print("Just calm the fuck down.")
                        return

        def start_dealer_play(self):
                print("[Beginning dealers play...]")
                self.ai.do_show(2)
                d_hand_val, d_aces = self.ai.hand_value()
                print("The dealers shown hand value is %s, plus %s ace" % (d_hand_val, d_aces) if d_aces > 0 else "The dealers hand value is %s" % d_hand_val)
                self.process_ai_move(self.ai.get_logic(self.player))

        def process_ai_move(self, move):
                print("----------------------------------------------------------")
                print("[Dealer decided to %s]" % move)
                
                if move == 'hit':
                        self.ai.add_cards(self.get_card(1), True)
                        if hand_win_bust(self.ai.hand_value()) > 0:
                                self.end_round(False) if hand_win_bust(self.ai.hand_value()) == 1 else self.end_round(True)
                                return
                        d_hand_val, d_aces = self.ai.hand_value()
                        print("The dealers hand value is %s, plus %s ace" % (d_hand_val, d_aces) if d_aces > 0 else "The dealers hand value is %s" % d_hand_val)
                        self.process_ai_move(self.ai.get_logic(self.player))
                elif move == 'stand':
                        # compare total to player total, determine winner
                        hand_val, aces = self.player.hand_value()
                        d_hand_val, d_aces = self.ai.hand_value()
                        if end_hand_val(d_hand_val, d_aces) > end_hand_val(hand_val, aces):
                                self.end_round(False)
                        else:
                                self.end_round(True)
                
        def get_card(self, num = 1):
                cards = []
                for x in range(0, num):
                        card, suit = self.deck.draw_card()
                        cards.append([card, suit])
                return cards

        def end_round(self, win, draw = False, bj = False):
                hand_val, aces = self.player.hand_value()
                d_hand_val, d_aces = self.ai.hand_value()
                print("End hands: Dealer: %s /// You: %s" % (end_hand_val(d_hand_val, d_aces), end_hand_val(hand_val, aces)))
                if win:
                        print("You won!" if not bj else "BLACKJACK!")
                        print("Your balance has increased by $%s" % self.player.bet)
                        self.player.wallet += int(self.player.bet)
                elif draw:
                        print("You tied!")
                else:
                        print("You lost.")
                        print("Your balance has decreased by $%s" % self.player.bet)
                        self.player.wallet -= int(self.player.bet)
                self.player.bet = 0
                if self.player.wallet < 1:
                        print("You're also out of money.  You really suck at this.")
                else:
                        print(" --------------------------------------------------------")
                        print("|                        AYYYY                           |")
                        print(" --------------------------------------------------------")
                        print("[New round]")
                        self.new_round()
                

class Deck():
        def __init__(self):
                # 11 = jack, 12 = queen, 13 = king, 14 = ace
                self.rand = SystemRandom()
                self.cards = {}
                self.suits = ['H', 'D', 'C', 'S']
                for suit in self.suits:
                        self.cards[suit] = [2,3,4,5,6,7,8,9,10,11,12,13,14]*decks

        def draw_card(self):
                suit = self.rand.choice(self.suits)
                card = self.rand.choice(self.cards[suit])
                self.remove_card(card, suit)
                return (card, suit)

        def remove_card(self, card, suit):
                self.cards[suit].remove(card)

class User():
        def __init__(self):
                self.wallet = 100
                self.wins = 0
                self.reset()
                
        def reset(self):
                self.bet = 0
                self.hand = {'H':[], 'D':[], 'C':[], 'S':[]}

        def hand_value(self):
                value = 0
                aces = 0
                for suits in self.hand:
                        for suit in suits:
                                for card in self.hand[suit]:
                                        if card == 14:
                                                aces += 1
                                        else:
                                                value += cardValues[card]
                return (value, aces)

        def add_cards(self, cards):
                for card in cards:
                        print("Got card: %s of %s" % (cardNames[card[0]], suitNames[card[1]]))
                        self.hand[card[1]].append(card[0])

class AI():
        def __init__(self):
                self.reset()
                self.wins = 0

        def reset(self):
                self.hide_hand = {'H':[], 'D':[], 'C':[], 'S':[]}
                self.show_hand = {'H':[], 'D':[], 'C':[], 'S':[]}

        def hand_value(self):
                value = 0
                aces = 0
                for suits in self.show_hand:
                        for suit in suits:
                                for card in self.show_hand[suit]:
                                        if card == 14:
                                                aces += 1
                                        else:
                                                value += cardValues[card]
                # return value is (<value without aces>, <number of aces>)
                return (value, aces)

        def add_cards(self, cards, show = False):
                for card in cards:
                        if show:
                                print("Dealer got card: %s of %s" % (cardNames[card[0]], suitNames[card[1]]))
                                self.show_hand[card[1]].append(card[0])
                        else:
                                self.hide_hand[card[1]].append(card[0])

        def do_show(self, type):
                # 1 - any card, 2 - all cards
                for suits in self.hide_hand:
                        for suit in suits:
                                for card in self.hide_hand[suit]:
                                        self.show_card(card, suit)
                                        if type == 1:
                                                return True

        def show_card(self, card, suit):
                # move a card from hidden to shown
                if card in self.hide_hand[suit]:
                        print("Dealer has shown card: %s of %s" % (cardNames[card], suitNames[suit]))
                        self.hide_hand[suit].remove(card)
                        self.show_hand[suit].append(card)

        def get_logic(self, player):
                hand_val, aces = self.hand_value()
                # enable soft17 to treat 17's with an ace as 7
                soft17 = True
                # enable yolo mode to let the dealer bet as long as they're winning
                yolo = False
                ehv = end_hand_val(hand_val, aces)
                opp_hv, opp_aces = player.hand_value()
                if ehv >= 21:
                        return 'stand'
                if ehv > end_hand_val(opp_hv, opp_aces):
                        return 'stand'
                elif ehv < end_hand_val(opp_hv, opp_aces) and yolo:
                        return 'hit'
                if aces > 0:
                        if (hand_val + (11*aces)) == 17:
                                return 'hit' if soft17 else 'stand'
                        elif (hand_val + (11*aces)) > 17:
                                total = hand_val
                                for ace in range(0, aces):
                                        total += 1
                                return 'stand' if total >= 17 else 'hit'
                        else:
                                return 'hit'
                else:
                        return 'hit' if hand_val < 17 else 'stand'
                


bgame = Game()
